<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid user-registration">
			<div class="row">
				<div class="col-xs-12">
					<div class="steps">
						<div class="step current">
							<div class="name">Step 1</div>
							<div class="info">Terms &amp; Conditions</div>
						</div>
						<div class="step">
							<div class="name">Step 2</div>
							<div class="info">Personal Information</div>
						</div>
						<div class="step">
							<div class="name">Step 3</div>
							<div class="info">Select Indicators</div>
						</div>
						<div class="step">
							<div class="name">Step 4</div>
							<div class="info">Open Leagues</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h1>Terms &amp; Conditions</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="well well-sm terms">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque suscipit, orci at blandit gravida, arcu nisl consectetur nibh, ut lobortis massa felis a nibh. Aenean sit amet arcu a velit viverra consequat id nec arcu. Sed nec dignissim justo. Donec posuere diam ac sapien ultricies vestibulum. Etiam nisl nisi, dapibus vel purus nec, fringilla bibendum lorem. Pellentesque luctus non libero quis tempus. Maecenas consequat molestie dictum. Donec adipiscing porttitor ipsum et suscipit. Etiam in consequat nunc. Praesent egestas, sem a hendrerit pellentesque, tortor tellus ornare diam, eget eleifend dolor neque nec nisi. Suspendisse ultrices placerat commodo.</p>

						<p>Sed sodales, arcu sed commodo suscipit, turpis diam venenatis eros, quis laoreet diam sem at sem. Vivamus at vehicula risus. Proin orci velit, tristique sed ipsum nec, elementum luctus quam. Maecenas condimentum purus a metus tempus dictum. Curabitur ac dolor porttitor, elementum enim quis, elementum lacus. Vestibulum ac fringilla mauris. Sed vulputate hendrerit nibh id consectetur. Ut auctor, justo eu tristique porta, lorem ligula auctor ante, a pulvinar enim lectus eget mi. Cras eget vehicula tellus, sit amet adipiscing nunc. Quisque risus est, mollis non turpis id, rutrum pulvinar velit.</p>

						<p>Cras ultricies velit urna, sodales dignissim neque lobortis eu. Integer quis sodales eros, at varius lectus. Duis sit amet sollicitudin lacus, sit amet gravida sem. Quisque a dui vulputate, dignissim metus vel, commodo nulla. Praesent euismod consequat aliquet. Nulla adipiscing mollis nunc vitae egestas. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque blandit mi quis metus porta, vitae accumsan libero pellentesque. Nam lobortis rhoncus neque et dignissim. Etiam nec gravida nunc. Donec est libero, vulputate et vulputate id, elementum sed nisi. Aenean pellentesque tortor massa, in pharetra enim ultrices nec.</p>

						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque suscipit, orci at blandit gravida, arcu nisl consectetur nibh, ut lobortis massa felis a nibh. Aenean sit amet arcu a velit viverra consequat id nec arcu. Sed nec dignissim justo. Donec posuere diam ac sapien ultricies vestibulum. Etiam nisl nisi, dapibus vel purus nec, fringilla bibendum lorem. Pellentesque luctus non libero quis tempus. Maecenas consequat molestie dictum. Donec adipiscing porttitor ipsum et suscipit. Etiam in consequat nunc. Praesent egestas, sem a hendrerit pellentesque, tortor tellus ornare diam, eget eleifend dolor neque nec nisi. Suspendisse ultrices placerat commodo.</p>

						<p>Sed sodales, arcu sed commodo suscipit, turpis diam venenatis eros, quis laoreet diam sem at sem. Vivamus at vehicula risus. Proin orci velit, tristique sed ipsum nec, elementum luctus quam. Maecenas condimentum purus a metus tempus dictum. Curabitur ac dolor porttitor, elementum enim quis, elementum lacus. Vestibulum ac fringilla mauris. Sed vulputate hendrerit nibh id consectetur. Ut auctor, justo eu tristique porta, lorem ligula auctor ante, a pulvinar enim lectus eget mi. Cras eget vehicula tellus, sit amet adipiscing nunc. Quisque risus est, mollis non turpis id, rutrum pulvinar velit.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center">
					<label><input type="checkbox"> I have read and accepted the Terms &amp; Conditions</label>
				</div>
			</div>
			<div class="row margin-top">
				<div class="col-xs-12 text-center">
					<!--<button type="submit" class="btn btn-primary disabled">&lt; Prev</button>-->
					<button type="submit" class="btn btn-primary">Next &gt;</button>
				</div>
			</div>

		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>