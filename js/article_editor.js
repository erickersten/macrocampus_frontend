$(function() {
	$('#article').editable({
		inlineMode: false,
		height: 450,
		borderColor: '#ddd',
		placeholder: ''
	});
});