<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid articles">
			<div class="row">
				<div class="col-xs-6">
					<h1>Articles</h1>
				</div>
				<div class="col-xs-6 margin-top text-right">
					<button type="submit" class="btn btn-success btn-lg">Write One</button>
				</div>
			</div>
			<!-- fila1 -->
			<div class="row">
				<div class="col-xs-12">
					<form class="form-inline basic-search" role="form">
						<div class="form-group">
							<input class="form-control input-sm" type="text"/>
							<button type="button" class="btn btn-primary btn-sm">Search</button>
							<a class="view-more">Advanced Search</a>
						</div>
					</form>
					
				</div>
			</div>
		
			<!-- fin fila1 -->
			<!-- fila2 -->
			<div class="row articles margin-top">
				<div class="col-xs-12">
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara03.jpg" width="68"/>
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara06.jpg" width="68"/>
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara02.jpg" width="68"/>
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara04.jpg" width="68"/>
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara01.jpg" width="68"/>
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 text-center"><a class="btn btn-primary">Load More</a></div>
			</div>
			<!-- fin fila2 -->

		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>