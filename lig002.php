<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid leagues enroll">
			<!-- fila1 -->
			<div class="row league">
				<div class="col-xs-2 text-center pr5">
					<p><strong>Institution</strong><br/><img src="img/liga02.jpg" class="responsive" width="100"/></p>
					<p><strong>Sponsored by</strong><br/><img src="img/sponsor01.jpg" class="responsive" width="100"/></p>
				</div>
				<div class="col-xs-6 margin-top">
					<h4>University XYZ  - US Macro Forecasting</h4>
					<p>Sponsored by University XYZ  University - School of Economics<br/>Closed to Invited participants<br/>From 01/04/2014 to 31/10/2014<br/></p>
					<p><strong>Course:</strong> Econ401<br/><strong>Season:</strong> Spring 2013</p>
					<h5>Indicators included:</h5>
					<p><strong>US:</strong> GDP - ISM - CPI - House Prices<br/><strong>Eurozone:</strong> Unemployment - CPI</p>
					<p class="margin-top">This League is available only to participants invited by the organizer.<br/>If you have received an invitation email, please enter the passcode</p>
				</div>
				<div class="col-xs-4 text-center date margin-top">
					<h4>Subscription Due Date</h4>
					<span>31/03/2014</span>
					<p>3 days remaining!</p>
					<h4 class="margin-top">Participants</h4>
					<p><strong>24</strong><br/>forecasters already enrolled</p>
				</div>
			</div>
			<div class="row">
			<div class="col-xs-8 col-xs-offset-2 margin-top">
				<form class="form-horizontal" role="form">
					<label class="control-label col-xs-2" for="inputSmall">Passcode</label>
						<div class="col-xs-5">
							<input class="form-control input-sm" type="password" id="inputSmall">
						</div>
					<div class="form-group">
						<div class="col-xs-offset-2 col-xs-10">
							<div class="checkbox">
							<label>
								<input type="checkbox"> I have and accepted the Leagues Terms & Conditions
							</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-6 text-center">
							<button type="submit" class="btn btn-primary">Enroll</button>
						</div>
					</div>
				
				</form>

			</div>
				
			</div>	
				</div>

			</div>
			<!-- fin fila4 -->
			<!-- fila 5 -->
			
			
			<!-- fin fila5 -->

		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>