<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid article">
			<div class="row">
				<div class="col-xs-12">
					<ol class="breadcrumb">
						<li><a href="#">Articles</a></li>
						<li class="active">ISM surprised on the upside</li>
					</ol>
				</div>
			</div>
			<div class="row margin-top">
				<div class="col-xs-8">
					<div class="row title">
						<div class="col-xs-1 pr5">
							<img src="img/cara03.jpg" width="68"  class="responsive"/>
						</div>
						<div class="col-xs-10 pl5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk published on 3/4/2014 3:55</p>
						</div>
					</div>
				<div class="row">
					<div class="col-xs-12 half-margin-top body">
						<p>The ISM manufacturing survey published last Friday once again delivered a positive surprise. The headline index inched up further from 56.2 in September to 56.4 in October, above consensus estimates (55.0), and to its highest level since April 2011.</p>
						<img src="img/com01.jpg" width="700"/>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October The details of the reports were less upbeat than the headline figure suggests. On the positive side, the ‘new orders’ sub-component remained pretty high (60.6, against 60.5 in September) and the ‘supplier deliveries’ and ‘inventories’ subindices increased m-o-m. However, although it remained pitched at a high level, the ‘production’ sub-index eased back further to 60.8, and the employment sub-index declined from 55.4 in September to 53.2 in October.</p>
					</div>
				</div>
				<div class="row margin-top half-margin-bottom">
					<div class="col-xs-6">
						<h4>Replies</h4>
					</div>
					<div class="col-xs-6 text-right">
						<a href="#" class="btn btn-success">Reply</a>
					</div>
				</div>
				<div class="row comment">
					<div class="col-xs-1 pr5">
						<img src="img/cara05.jpg" width="45" alt="">
					</div>
					<div class="col-xs-11 pl5">
						<h5>User Name on 3/4/2014</h5>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October</p>
					</div>
				</div>
				<div class="row comment">
					<div class="col-xs-1 pr5">
						<img src="img/cara06.jpg" width="45" alt="">
					</div>
					<div class="col-xs-11 pl5">
						<h5>User Name on 3/4/2014</h5>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October</p>
					</div>
				</div>
				<div class="row comment">
					<div class="col-xs-1 pr5">
						<img src="img/cara01.jpg" width="45" alt="">
					</div>
					<div class="col-xs-11 pl5">
						<h5>User Name on 3/4/2014</h5>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October</p>
					</div>
				</div>
				<div class="row comment">
					<div class="col-xs-1 pr5">
						<img src="img/cara02.jpg" width="45" alt="">
					</div>
					<div class="col-xs-11 pl5">
						<h5>User Name on 3/4/2014</h5>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October</p>
					</div>
				</div>
				<div class="row comment">
					<div class="col-xs-1 pr5">
						<img src="img/cara03.jpg" width="45" alt="">
					</div>
					<div class="col-xs-11 pl5">
						<h5>User Name on 3/4/2014</h5>
						<p>ISM manufacturing: ‘employment’ sub-component weaker in October</p>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 text-center">
						<a href="#" class="view-more">Load More</a>
					</div>
				</div>
			</div>
			<aside class="col-xs-4">
				<div class="row">
					<div class="col-xs-12">
						<h4>Related Articles</h4>
					</div>
				</div>
				<div class="related">
					<!-- start -->
					<div class="row article">
						<div class="col-xs-2">
							<img src="img/cara01.jpg" width="30" alt="">
						</div>
						<div class="col-xs-10">
							<h5>ISM manufacturing: ‘employment’</h5>
							<p>By Francis U.</p>
						</div>
					</div>
					<!-- end -->
					<!-- start -->
					<div class="row article">
						<div class="col-xs-2">
							<img src="img/cara03.jpg" width="30" alt="">
						</div>
						<div class="col-xs-10">
							<h5>ISM manufacturing: ‘employment’</h5>
							<p>By John S.</p>
						</div>
					</div>
					<!-- end -->
					<!-- start -->
					<div class="row article">
						<div class="col-xs-2">
							<img src="img/cara05.jpg" width="30" alt="">
						</div>
						<div class="col-xs-10">
							<h5>Prices: ‘employment’</h5>
							<p>By Martha T.</p>
						</div>
					</div>
					<!-- end -->
				</div>
			
				<div class="row forecasters">
					<div class="col-xs-12">
						<h4>Top Forecasters</h4>
					</div>
				</div>
				<div class="col-xs-6 widget-top3">
					<h6>Activity</h6>
					<ol>
						<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Flavia</span></li>
						<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Sarah</span></li>
						<li><img src="img/cara01.jpg" width="45" alt=""><span class="author">John</span></li>
					</ol>
				</div>
				<div class="col-xs-6 widget-top3">
					<h6>Consumer</h6>
					<ol>
						<li><img src="img/cara02.jpg" width="45" alt=""><span class="author">Steve</span></li>
						<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">Mike</span></li>
						<li><img src="img/cara04.jpg" width="45" alt=""><span class="author">Carol</span></li>
					</ol>
				</div>


			</aside>



		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>