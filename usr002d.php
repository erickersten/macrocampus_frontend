<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid user-registration">
			<div class="row">
				<div class="col-xs-12">
					<div class="steps">
						<div class="step past">
							<div class="name">Step 1</div>
							<div class="info">Terms &amp; Conditions</div>
						</div>
						<div class="step past">
							<div class="name">Step 2</div>
							<div class="info">Personal Information</div>
						</div>
						<div class="step past">
							<div class="name">Step 3</div>
							<div class="info">Select Indicators</div>
						</div>
						<div class="step current">
							<div class="name">Step 4</div>
							<div class="info">Open Leagues</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h1>Active &amp; Open Leagues</h1>
				</div>
			</div>
			<div class="row leagues">
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga01.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>AEA Macro Challenge </h5>
							<p>American Economic Association<br/>
							From 1/8/2013 to 15/11/2013<br/>
							Enrolled participants: 24<br/>
							Prize: US$ 1000 on grants
						</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Suscribe</button>
						</div>
					</div>
					
				</div>
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga02.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>University XYZ  - Course Econ 401 - Macro Forecasting</h5>
							<p>University XYZ  University<br/>
							From 1/8/2013 to 15/11/2013<br/>
							Professor John Smith</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-success">Suscribed</button>
						</div>
					</div>
					
				</div>
			</div>
			<hr>
			<div class="row leagues">
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga03.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>Forecast Brasil - Fundacao Getulio Vargas</h5>
							<p>Sponsored by University XYZ  University - School of Economics<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: 50 credits</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Suscribe</button>
						</div>
					</div>
					
				</div>
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga04.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>NYU - Econometrics 110</h5>
							<p>Sponsored by University XYZ  University - School of Economics<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: 50 credits</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Suscribed</button>
						</div>
					</div>
					
				</div>
			</div>
			<hr>
			<div class="row leagues">
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga05.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>Talent Scouting- Top Tier 1 Bank</h5>
							<p>Sponsored by University XYZ  University - School of Economics<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: 50 credits</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Suscribe</button>
						</div>
					</div>
					
				</div>
				<div class="col-xs-6 league">
					<div class="row">
						<div class="col-xs-2">
							<img src="img/liga06.jpg" width="80" alt="">
						</div>
						<div class="col-xs-7">
							<h5>Global Institution- Macro Challenge</h5>
							<p>Sponsored by University XYZ  University - School of Economics<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: 50 credits</p>
						</div>
						<div class="col-xs-3">
							<button type="submit" class="btn btn-default">Suscribed</button>
						</div>
					</div>
					
				</div>
			</div>
			<hr>
			<!-- fila 4 -->
			<div class="row margin-top">
				<div class="col-xs-12 text-center">
					<button type="submit" class="btn btn-primary">< Previous </button>
					<button type="submit" class="btn btn-primary">Finish</button>
				</div>
			</div>
			<!-- fin fila4 -->
		
		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>