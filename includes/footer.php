<footer>
	<section class="container-fluid">
		<ul>
			<li><a href="#" class="icon icon-facebook"></a></li>
			<li><a href="#" class="icon icon-twitter"></a></li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Terms &amp; Conditions</a></li>
			<li><a href="#">Privacy</a></li>
			<li><a href="#">Contact</a></li>
		</ul>
	</section>
</footer>