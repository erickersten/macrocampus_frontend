<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid ranking">
			<div class="row">
				<div class="col-xs-12">
					<h1>Ranking</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3">
					<div class="list-group">
						<a class="list-group-item active">US<span class="badge">14</span></a>
						<a class="list-group-item">Eurozone<span class="badge">2</span></a>
						<a class="list-group-item">Germany<span class="badge">1</span></a>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">General</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>170</td>
										<td>17<sup>th</sup></td>
									</tr>
									<tr>
										<td>John</td>
										<td>210</td>
										<td>1<sup>st</sup></td>
									</tr>
									<tr>
										<td>Mike</td>
										<td>208</td>
										<td>2<sup>nd</sup></td>
									</tr>
									<tr>
										<td>Peter</td>
										<td>203</td>
										<td>3<sup>rd</sup></td>
									</tr>
									<tr>
										<td>Ben</td>
										<td>198</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Alan</td>
										<td>193</td>
										<td>5<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
				<div class="col-xs-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">Markets</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>98</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Alan</td>
										<td>108</td>
										<td>1<sup>th</sup></td>
									</tr>
									<tr>
										<td>Ben</td>
										<td>49</td>
										<td>2<sup>th</sup></td>
									</tr>
									<tr>
										<td>Janet</td>
										<td>103</td>
										<td>3<sup>th</sup></td>
									</tr>
									<tr>
										<td>Mike</td>
										<td>98</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Peter</td>
										<td>88</td>
										<td>5<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
				<div class="col-xs-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">Activity</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>98</td>
										<td>2<sup>nd</sup></td>
									</tr>
									<tr>
										<td>Peter</td>
										<td>120</td>
										<td>1<sup>st</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>108</td>
										<td>3<sup>rd</sup></td>
									</tr>
									<tr>
										<td>Mike</td>
										<td>103</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Ben</td>
										<td>98</td>
										<td>5<sup>th</sup></td>
									</tr>
									<tr>
										<td>Alan</td>
										<td>93</td>
										<td>6<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
			</div>
			<div class="row">
				<!--
				<div class="col-xs-3"></div>
				aplicandole un offset de 3 a la columna siguiente
				no hace falta meter el div vacio
				-->
				<div class="col-xs-3 col-xs-offset-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">Prices</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>96</td>
										<td>17<sup>st</sup></td>
									</tr>
									<tr>
										<td>John</td>
										<td>120</td>
										<td>1<sup>nd</sup></td>
									</tr>
									<tr>
										<td>Mike</td>
										<td>108</td>
										<td>2<sup>rd</sup></td>
									</tr>
									<tr>
										<td>Peter</td>
										<td>102</td>
										<td>3<sup>th</sup></td>
									</tr>
									<tr>
										<td>Ben</td>
										<td>94</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Alan</td>
										<td>88</td>
										<td>5<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
				<div class="col-xs-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">General</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>18</td>
										<td>60<sup>th</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>50</td>
										<td>1<sup>st</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>49</td>
										<td>2<sup>nd</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>48</td>
										<td>3<sup>rd</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>47</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>46</td>
										<td>5<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
				<div class="col-xs-3">
					<div class="panel panel-primary no-padding">
						<div class="panel-heading">
							<h3 class="panel-title">General</h3>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered ranking">
								<thead>
									<tr>
										<th>Name</th>
										<th>Score</th>
										<th>Rank</th>
									</tr>
								</thead>
								<tbody>
									<tr class="info">
										<td>YOU</td>
										<td>18</td>
										<td>60<sup>th</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>50</td>
										<td>1<sup>st</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>49</td>
										<td>2<sup>nd</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>48</td>
										<td>3<sup>rd</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>47</td>
										<td>4<sup>th</sup></td>
									</tr>
									<tr>
										<td>Juan</td>
										<td>46</td>
										<td>5<sup>th</sup></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- cierra col -->
			</div>
		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>