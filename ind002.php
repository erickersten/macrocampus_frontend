<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid indicator-details">
			<div class="row">
				<div class="col-xs-6">
					<ol class="breadcrumb">
						<li><a href="#">Indicators</a></li>
						<li class="active">ISM Manufacturing <span class="about">(<a href="#">About this indicator</a>)</span></li>
					</ol>
				</div>
				<div class="col-xs-6 text-right">
					<a href="#" class="btn btn-primary">&lt; Back to list</a>
				</div>
			</div>
			<div class="row info">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-2 special pl5 pr5">
							<div class="well well-sm period">
								<h5>Period</h5>
								<div class="big">JAN<br/>2014</div>
								<div class="view-all"><a href="#">View all</a></div>
							</div>
						</div>
						<div class="col-xs-2 special pl5 pr5">
							<div class="well well-sm release">
								<h5>Release Date</h5>
								<div class="big">16/01/14<br/>08:30</div>
							</div>
						</div>
						<div class="col-xs-2 special pl5 pr5">
							<div class="well well-sm">
								<div class="row">
									<div class="col-xs-5 text-center mean">
										<h5>Mean</h5>
										<div class="big">55.3</div>
									</div>
									<div class="col-xs-7 text-center yours">
										<h5>Yours</h5>
										<a href="" class="btn btn-default btn-sm">View Prev.</a><a href="" class="btn btn-default btn-sm">Make Yours</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-2 special pl5 pr5">
							<div class="well well-sm current">
								<h5>Current Value</h5>
								<div class="big">52.1</div>
							</div>
						</div>
						<div class="col-xs-2 special pl5 pr5">
							<div class="well well-sm ranking">
								<h5>Ranking</h5>
								<ul>
									<li>1<sup>st</sup> John</li>
									<li>2<sup>nd</sup> Brian</li>
									<li>3<sup>rd</sup> Ana</li>
								</ul>
								<div class="view-all"><a href="#">View all</a></div>
							</div>
						</div>
					</div>
				</div>
				
				
				
			</div>
			<div class="row graphics">
				<div class="col-xs-6">
					<h4>Mean Evolution</h4>
					<img src="img/grafico01.jpg" width="550"/>
				</div>
				<div class="col-xs-6">
					<h4>Distribution for this forecast</h4>
					<img src="img/grafico02.jpg" width="550"/>
				</div>
				<div class="col-xs-6">
					<h4>Surprise</h4>
					<img src="img/grafico03.jpg" width="550"/>
				</div>
				<div class="col-xs-6">
					<h4>ISM - Actual vs. Forecast</h4>
					<img src="img/grafico04.jpg" width="550"/>
				</div>
			</div>
			<div class="row margin-top half-margin-bottom">
				<div class="col-xs-6">
					<h4>Latest Articles</h4>
				</div>
				<div class="col-xs-6 text-right">
					<a href="#" class="btn btn-success btn-lg">Write one</a>
				</div>
			</div>
			<div class="row articles">
				<div class="col-xs-12">
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara03.jpg" width="68" alt="">
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara04.jpg" width="68" alt="">
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Carolina Perez</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row article">
						<div class="col-xs-1">
							<img src="img/cara01.jpg" width="68" alt="">
						</div>
						<div class="col-xs-5">
							<h5>ISM surpised on the upside</h5>
							<p>by Hernan Kisluk</p>
						</div>
						<div class="col-xs-2 text-center">
							<span class="icon icon-table"></span> Jun-14 3:55
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-eye"> </span> 52
						</div>
						<div class="col-xs-1 text-right">
							<span class="icon icon-bubble"></span> 5
						</div>
						<div class="col-xs-2 text-right">
							<span class="icon icon-tag"></span> US Activity
						</div>
					</div>
					<!-- article end -->
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 view-all"><a href="#">Load More...</a></div>
			</div>
			<div class="row data margin-top">
				<div class="col-xs-12">
					<h4>Data description</h4>
					<p>An index based on surveys of more than 300 manufacturing firms by the Institute of Supply Management. The ISM Manufacturing Index monitors employment, production inventories, new orders and supplier deliveries. A composite diffusion index is created that monitors conditions in national manufacturing based on the data from these surveys.  </p>
				</div>
			</div>
		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>