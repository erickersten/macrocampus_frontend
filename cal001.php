<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid user-registration">
			<div class="row">
				<div class="col-xs-12">
					<h1>Calendar</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="calendar">
						<div class="month"><a class="prev glyphicon glyphicon-chevron-left"></a>March 2014<a class="next glyphicon glyphicon-chevron-right"></a></div>
						<div class="week-selector">
							<a href="#" class="btn btn-primary btn-sm prev">&laquo; Prev week</a>
							<a href="#" class="btn btn-primary btn-sm next">Next Week &raquo;</a>
						</div>
						<div class="columns">
							<div class="column">
								<div class="dayname">MONDAY 12<sup>th</sup></div>
								<div class="indicators">
									<!-- item start -->
									<div class="item">
										<div class="name">ISM Manufacturing </div>
										<div class="info">
											<span class="hour">8:30</span>
											<span class="status forecasted">forecasted</span>
										</div>
										<div class="values">
											<table>
												<thead>
													<tr>
														<th>Period</th>
														<th>Fcst.</th>
														<th>Yours</th>
														<th>Actual</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>Mar. 14</td>
														<td>55.2</td>
														<td>53</td>
														<td>52.9</td>
													</tr>
												</tbody>
											</table>
										</div>
								</div>
									<!-- item end -->
									 <!-- item start -->
									<div class="item">
										<div class="name">Activity</div>
										<div class="info">
											<span class="hour">10:30</span>
											<span class="status missed">missed</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>52.9</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
								</div>
							</div>
							<div class="column today">
								<div class="dayname">TUESDAY 13<sup>th</sup></div>
								<div class="indicators">
									<!-- item start -->
									<div class="item">
										<div class="name">Construction</div>
										<div class="info">
											<span class="hour">10:30</span>
											<span class="status pending">pending</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											 </thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>-</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
								</div>
							</div>
							<div class="column">
								<div class="dayname">WEDNESDAY 14<sup>th</sup></div>
								<div class="indicators">
									 <!-- item start -->
									<div class="item">
										<div class="name">Durable Goods</div>
										<div class="info">
											<span class="hour">10:30</span>
											<span class="status pending">pending</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>52.9</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
								</div>
							</div>
							<div class="column">
								<div class="dayname">THURSDAY 15<sup>th</sup></div>
								<div class="indicators"></div>
							</div>
							<div class="column">
								<div class="dayname">FRIDAY 16<sup>th</sup></div>
								<div class="indicators">
									 <!-- item start -->
									<div class="item">
										<div class="name">Fed Rate Decision</div>
										<div class="info">
											<span class="hour">08:30</span>
											<span class="status pending">pending</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>52.9</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
									 <!-- item start -->
									<div class="item">
										<div class="name">import Price Index YOY</div>
										<div class="info">
											<span class="hour">10:30</span>
											<span class="status pending">pending</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>52.9</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
									 <!-- item start -->
									<div class="item">
										<div class="name">GDP YoY</div>
										<div class="info">
											<span class="hour">12:30</span>
											<span class="status forecasted">forecasted</span>
										</div>
										<div class="values">
										<table>
											<thead>
												<tr>
													<th>Period</th>
													<th>Fcst.</th>
													<th>Yours</th>
													<th>Actual</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Mar. 14</td>
													<td>55.2</td>
													<td>-</td>
													<td>52.9</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
									<!-- item end -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>