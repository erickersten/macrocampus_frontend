<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid podium">
			<h1>Ranking</h1>
			<div class="row">
				<div class="col-xs-3">
					<div class="list-group">
						<a class="list-group-item active">US<span class="badge">14</span></a>
						<a class="list-group-item">Eurozone<span class="badge">2</span></a>
						<a class="list-group-item">Germany<span class="badge">1</span></a>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="row">
						<div class="col-xs-12 text-center">
							<h3>US General Ranking</h3>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-12 text-center">
							<div class="person row first">
								<div class="col-xs-4 pr5">
									<img src="img/cara01.jpg" width="80" alt="" class="responsive">
								</div>
								<div class="col-xs-8 pl5">
									<h4>Juan Gomez</h4>
									<span>1<sup>st</sup></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-6 text-center">
							<div class="person row">
								<div class="col-xs-4 pr5">
									<img src="img/cara03.jpg" width="80" alt="" class="responsive">
								</div>
								<div class="col-xs-8 pl5">
									<h4>Jack Chen</h4>
									<span>2<sup>nd</sup></span>
								</div>
							</div>
						</div>
						<div class="col-xs-6 text-center">
							<div class="person row">
								<div class="col-xs-4 pr5">
									<img src="img/cara02.jpg" width="80" alt="" class="responsive">
								</div>
								<div class="col-xs-8 pl5">
									<h4>John Smith</h4>
									<span>3<sup>rd</sup></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-3 widget-top3">
							<h6>Activity</h6>
							<ol>
								<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">John</span></li>
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>Consumer</h6>
							<ol>
								<li><img src="img/cara04.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">John</span></li>
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>Labor Market</h6>
							<ol>
								<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">John</span></li>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara04.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>Federal Reserve</h6>
							<ol>
								<li><img src="img/cara02.jpg" width="45" alt=""><span class="author">John</span></li>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								<li><img src="img/cara01.jpg" width="45" alt=""><span class="author">John</span></li>
							</ol>
						</div>
					</div>
					<div class="row margin-top">
						<div class="col-xs-3 widget-top3">
							<h6>Prices</h6>
							<ol>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara02.jpg" width="45" alt=""><span class="author">John</span></li>
								<li><img src="img/cara04.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>Housing</h6>
							<ol>
								<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">John</span></li>
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>External Sector</h6>
							<ol>
								<li><img src="img/cara01.jpg" width="45" alt=""><span class="author">John</span></li>
								<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara04.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								
							</ol>
						</div>
						<div class="col-xs-3 widget-top3">
							<h6>Labor Market</h6>
							<ol>
								<li><img src="img/cara05.jpg" width="45" alt=""><span class="author">Sara</span></li>
								<li><img src="img/cara06.jpg" width="45" alt=""><span class="author">Flavia</span></li>
								<li><img src="img/cara03.jpg" width="45" alt=""><span class="author">John</span></li>
							</ol>
						</div>
					</div>


				</div>
			
			</div>
		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>