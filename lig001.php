<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid leagues">
			<div class="row">
				<div class="col-xs-6">
					<h1>Active Leagues</h1>
				</div>
				<div class="col-xs-6 text-right margin-top">
					<a class="btn btn-success btn-lg">Create new League</a>
				</div>
			</div>
			<!-- fila1 -->
			<div class="row leagues-list">
				<div class="col-xs-12">
					<!-- article start -->
					<div class="row league">
						<div class="col-xs-1 text-center logo">
							<p>&nbsp;<br/><img src="img/liga02.jpg" class="responsive" width="80"/></p>
						</div>
						<div class="col-xs-1 text-center logo">
							<p>Sponsored by<br/><img src="img/sponsor01.jpg" class="responsive" width="80"/></p>
						</div>
						<div class="col-xs-4">
							<h5>University XYZ  - US Macro Forecasting</h5>
							<p>Sponsored by University XYZ  University - School of Economics<br/>
							Closed to Invited participants<br/>
							From 01/04/2014 to 31/10/2014<br/>	
							</p>
						</div>
						<div class="col-xs-2 text-center date">
							<h5>Subscription Due Date</h5>
							<span>31/03/2014</span>
							<p>3 days remaining!</p>
						</div>
						<div class="col-xs-1 text-center participants">
							<h5>Participants</h5>
							<p>24</p>
						</div>
						<div class="col-xs-2 text-center type">
							<h5>Admission Type</h5>
							<p><span class="icon icon-lock"></span></p>
						</div>
						<div class="col-xs-1 text-center monitor">
							<h5>Monitor</h5>
							<p><span class="icon icon-binoculars"></span></p>
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row league">
						<div class="col-xs-1 text-center logo">
							<p>&nbsp;<br/><img src="img/liga06.jpg" class="responsive" width="80"/></p>
						</div>
						<div class="col-xs-1 text-center logo"></div>
						<div class="col-xs-4">
							<h5>Global Institution Macro Challenge</h5>
							<p>Sponsored by Global Institution Institute - Chicago Society<br/>
							Closed to Invited participants<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: awards for best three forecasters
							</p>
						</div>

						<div class="col-xs-2 text-center date">
							<h5>Subscription Due Date</h5>
							<span>31/03/2014</span>
							<p>3 days remaining!</p>
						</div>
						<div class="col-xs-1 text-center participants">
							<h5>Participants</h5>
							<p>47</p>
						</div>
						<div class="col-xs-2 text-center type">
							<h5>Admission Type</h5>
							<p><span class="icon icon-lock"></span></p>
						</div>
						<div class="col-xs-1 text-center monitor">
							<h5>Monitor</h5>
							<p><span class="icon icon-binoculars"></span></p>
						</div>
					</div>
					<!-- article end -->
					<!-- article start -->
					<div class="row league">
						<div class="col-xs-1 text-center logo">
							<p>&nbsp;<br/><img src="img/liga05.jpg" class="responsive" width="80"/></p>
						</div>
						<div class="col-xs-1 text-center logo"></div>
						<div class="col-xs-4">
							<h5>Economic Research Foundation- Talent Scouting</h5>
							<p>Sponsored by Top Tier 1 Bank<br/>
							Open with admission<br/>
							From 01/04/2014 to 31/10/2014<br/>
							Prize: internship for the best forecaster
							</p>
						</div>
						<div class="col-xs-2 text-center date">
							<h5>Subscription Due Date</h5>
							<span>31/03/2014</span>
							<p>3 days remaining!</p>
						</div>
						<div class="col-xs-1 text-center participants">
							<h5>Participants</h5>
							<p>108</p>
						</div>
						<div class="col-xs-2 text-center type">
							<h5>Admission Type</h5>
							<p>
								<span class="icon icon-unlocked"></span><a class="btn btn-success">Enroll</a>
							</p>
						</div>
						<div class="col-xs-1 text-center monitor">
							<h5>Monitor</h5>
							<p><span class="icon icon-binoculars"></span></p>
						</div>
					</div>
					<!-- article end -->
				</div>
			</div>
			<!-- fin fila1 -->

		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>