<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Macro Campus</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Open+Sans:400,300,700' rel='stylesheet' type='text/css'/>
	<link rel="stylesheet" href="css/macrocampus.css"/>
</head>
<body>
	<div class="page-wrap">
		<?php include('includes/header.php'); ?>
		<section class="container-fluid ranking">
			<div class="row">
				<div class="col-xs-12">
					<h1>Scores & Rankings</h1>
				</div>
			</div>
			<!-- fila2 -->
			<div class="row margin-top">
				<div class="col-xs-12">
					<table class="table table-striped table-bordered ranking">
						<thead>
							<tr>
								<th>General</th>
								<th class="text-center">#Estimates</th>
								<th class="text-center">Score</th>
								<th class="text-center">Ranking</th>
								<th class="text-center">Evolution</th>
								<th class="text-center">Forecasts</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="first-level"><span class="icon-caret-down"></span> US</td>
								<td class="text-center">18</td>
								<td class="text-center">255</td>
								<td class="text-center">5<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="second-level"><span class="icon-caret-down"></span> Economic Activity</td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="third-level"><span>ISM</span></td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats" data-toggle="modal" data-target=".bs-example-modal-sm"></a>
								</td>
								<td class="text-center">
									<a href="#" class="icon icon-calendar"></a>
								</td>
							</tr>
							<tr>
								<td class="second-level"><span class="icon-caret-right"></span> Construction</td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="first-level"><span class="icon-caret-right"></span> UK</td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="first-level"><span class="icon-caret-right"></span> Italy</td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td class="first-level"><span class="icon-caret-right"></span> Germany</td>
								<td class="text-center">4</td>
								<td class="text-center">94</td>
								<td class="text-center">4<sup>th</sup></td>
								<td class="text-center">
									<a href="#" class="icon icon-stats"></a>
								</td>
								<td class="text-center"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- fin fila2 -->
			<!-- fila3 -->
			<div class="row margin-top">
				<div class="col-xs-12">
					<table class="table table-striped table-bordered ranking">
						<thead>
							<tr>
								<th>League</th>
								<th class="text-center">#Estimates</th>
								<th class="text-center">Score</th>
								<th class="text-center">Evolution</th>
								<th class="text-center">Forecasts</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><img src="img/liga02.jpg" width="25" alt=""> University XYZ  - US Macro Forecasting</td>
								<td class="text-center">18</td>
								<td class="text-center">198</td>
								<td class="text-center"><a href="#" class="icon icon-stats"></a></td>
								<td class="text-center"></td>
							</tr>
							<tr>
								<td><img src="img/liga05.jpg" width="25" alt=""> Gold Sachas - Talent Scouting</td>
								<td class="text-center">18</td>
								<td class="text-center">198</td>
								<td class="text-center"><a href="#" class="icon icon-stats"></a></td>
								<td class="text-center"></td>
							</tr>

						</tbody>
					</table>
				</div>
			</div>
			<!-- fin fila3 -->


			<!-- fila3 -->
			<!-- fin fila3 -->
			<!-- fila 4 -->
			<!-- fin fila4 -->

		</section>
	</div>
	<?php include('includes/footer.php'); ?>
	<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Evolution</h4>
				</div>
				<div class="modal-body">
					<img src="img/grafico01.jpg" alt="">
				</div>
			</div><!-- /.modal-content -->
	</div>
	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script src="js/libs/bootstrap/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
</body>
</html>