<header class="navbar">
	<section class="container-fluid">
		<div class="row">
			<div class="col-xs-3"><img src="img/logo.png" width="273px" /></div>
			<div class="col-xs-9 pull-right">
			<ul class="nav nav-pills">
				<li ><a href="#">Indicators</a></li>
				<li><a href="#">Leagues</a></li>
				<li><a href="#">Forecasters</a></li>
				<li><a href="#">Rankings</a></li>
				<li><a href="#">Articles</a></li>
				<li>
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> Sign In <span class="caret"></span></a>
						<div class="dropdown-menu login">
							<form role="form">
							<div class="form-group">
								<label for="username">Username</label>
								<input type="text" class="form-control input-sm" id="username" placeholder="Enter Username">
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control input-sm" id="password" placeholder="Password">
							</div>
							<div class="form-actions"><button type="submit" class="btn btn-primary">Submit</button></div>
							</form>
						</div>
				</li>
			</ul>
			</div>
		</div>
	</section>
</header>